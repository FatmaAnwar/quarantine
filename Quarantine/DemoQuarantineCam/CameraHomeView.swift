//
//  ViewController.swift
//  Quarantine
//
//  Created by Fatma Anwar on 3/26/20.
//  Copyright © 2020 Cappsule. All rights reserved.
//

import UIKit
import AVFoundation
import TransitionButton
import UIKit
import AVFoundation
import TransitionButton

class CameraHomeView: QuarantineViewController, QuarantineCamViewControllerDelegate  {
    
    @IBOutlet weak var closeButton: TransitionButton!
    @IBOutlet weak var stayHomeImage: UIImageView!
    @IBOutlet weak var quarantineImage: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var captureButton    : QuarantineRecordButton!
    @IBOutlet weak var flipCameraButton : UIButton!
    @IBOutlet weak var flashButton      : UIButton!    
    @IBOutlet weak var shareButton: UIButton!
    var count = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // defaultCamera = .front
        quarantineImage.image = UIImage.gif(name: "quarantine_camera")
        stayHomeImage.image = UIImage.gif(name: "stay_home")
        stayHomeImage.clipsToBounds = true
        stayHomeImage.contentMode = .scaleAspectFit
        shouldPrompToAppSettings = true
        cameraDelegate = self
        maximumVideoDuration = 10.0
        shouldUseDeviceOrientation = true
        allowAutoRotate = true
        audioEnabled = true
        flashMode = .auto
        captureButton.buttonEnabled = false
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
        if titleText.center != CGPoint(x:50, y:10) {
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: .curveLinear, animations: { () -> Void in
                self.titleText.center = CGPoint(x:100, y:70)
            }, completion: nil)
        }
        
    }
    
    @IBAction func buttonAction(_ button: TransitionButton) {
        // session.stopRunning()
        button.startAnimation() // 2: Then start the animation when the user tap the button
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            
            sleep(1) // 3: Do your networking task or background work here.
            
            DispatchQueue.main.async(execute: { () -> Void in
                // 4: Stop the animation, here you have three options for the `animationStyle` property:
                // .expand: useful when the task has been compeletd successfully and you want to expand the button and transit to another view controller in the completion callback
                // .shake: when you want to reflect to the user that the task did not complete successfly
                // .normal
                button.stopAnimation(animationStyle: .expand, completion: {
                    let newViewController = self.storyboard!.instantiateViewController(withIdentifier: "ThankYouView")
                    self.present(newViewController, animated: true, completion: nil)
                    
                })
            })
        })
    }
    
    @IBAction func shareBtnPressed(_ sender: Any) {
        if(Constants.videoURL != "")
        {  let localVideoPath = Constants.videoURL // "your_video_path_here..."
            let videoURL = URL(fileURLWithPath: localVideoPath)
            let activityItems: [Any] = [videoURL, "Your Vedio"]
            let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activityController.popoverPresentationController?.sourceView = view
            activityController.popoverPresentationController?.sourceRect = view.frame
            self.present(activityController, animated: true, completion: nil)
        }
        else{
            
            let alert = UIAlertController(title: "", message: "Please record a vedio first!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func update() {
        if(Constants.counter > 0) {
            
            self.counterLabel.text = "\(Constants.counter-1)"
            Constants.counter = Constants.counter - 1
        }
        else if (Constants.counter == 0){
            self.counterLabel.text = "\(Constants.counter-1)"
            Constants.counter = Constants.counter - 1
        }
        else{
            self.counterLabel.isHidden = true
        }
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureButton.delegate = self
    }
    
    func swiftyCamSessionDidStartRunning(_ swiftyCam: QuarantineViewController) {
        print("Session did start running")
        captureButton.buttonEnabled = true
    }
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: QuarantineViewController) {
        print("Session did stop running")
        captureButton.buttonEnabled = false
    }
    
    //    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
    //        let newVC = PhotoViewController(image: photo)
    //        self.present(newVC, animated: true, completion: nil)
    //    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didBeginRecordingVideo camera: QuarantineViewController.CameraSelection) {
        print("Did Begin Recording")
        captureButton.growButton()
        hideButtons()
        counterLabel.isHidden = false
        counterLabel.text = "10"
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFinishRecordingVideo camera: QuarantineViewController.CameraSelection) {
        print("Did finish Recording")
        captureButton.shrinkButton()
        showButtons()
        Constants.counter = 10
        counterLabel.isHidden = true
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFinishProcessVideoAt url: URL) {
        let newVC = VideoViewController(videoURL: url)
        self.present(newVC, animated: true, completion: nil)
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFocusAtPoint point: CGPoint) {
        print("Did focus at point: \(point)")
        focusAnimationAt(point)
    }
    
    func swiftyCamDidFailToConfigure(_ swiftyCam: QuarantineViewController) {
        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didChangeZoomLevel zoom: CGFloat) {
        print("Zoom level did change. Level: \(zoom)")
        print(zoom)
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didSwitchCameras camera: QuarantineViewController.CameraSelection) {
        print("Camera did change to \(camera.rawValue)")
        print(camera)
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFailToRecordVideo error: Error) {
        print(error)
    }
    
    @IBAction func cameraSwitchTapped(_ sender: Any) {
        switchCamera()
    }
    
    @IBAction func toggleFlashTapped(_ sender: Any) {
        //flashEnabled = !flashEnabled
        // toggleFlashAnimation()
    }
}


// UI Animations
extension CameraHomeView {
    
    fileprivate func hideButtons() {
        UIView.animate(withDuration: 0.25) {
            self.shareButton.alpha = 0.0
            self.flipCameraButton.alpha = 0.0
            self.closeButton.alpha = 0.0
        }
    }
    
    fileprivate func showButtons() {
        UIView.animate(withDuration: 0.25) {
            self.shareButton.alpha = 1.0
            self.flipCameraButton.alpha = 1.0
            self.closeButton.alpha = 1.0
        }
    }
    
    fileprivate func focusAnimationAt(_ point: CGPoint) {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) { (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }) { (success) in
                focusView.removeFromSuperview()
            }
        }
    }
    
    fileprivate func toggleFlashAnimation() {
        //flashEnabled = !flashEnabled
        if flashMode == .auto{
            flashMode = .on
            flashButton.setImage(#imageLiteral(resourceName: "flash"), for: UIControl.State())
        }else if flashMode == .on{
            flashMode = .off
            flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControl.State())
        }else if flashMode == .off{
            flashMode = .auto
            flashButton.setImage(#imageLiteral(resourceName: "flashauto"), for: UIControl.State())
        }
    }
}

