//
//  Constants.swift
//  Quarantine
//
//  Created by Fatma Anwar on 3/25/20.
//  Copyright © 2020 Fatma Anwar. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Constants {
    // MARK: --Posts
    
    static let NEAR_U = "Jeet"
    static var counter = -10
    static var  hideCounter = true
    static var  startRecord = false
    static  var videoURLLink = URL(string: "")
    static var videoURL = ""
    
    // MARK: -Constants values
    
    static let Background_COLOR = UIColor(hexFromString: "396A7F")
    static let Background_COLORwithAlpha = UIColor(hexFromString: "396A7F",alpha: 0.5)
    static let Logo_COLOR = UIColor(hexFromString: "7BF6DA")
    //  static let Logo_COLOR = UIColor(hexFromString: "7BF6DA")
    static let text_COLOR = UIColor(hexFromString: "77F4D6")
    
    
    
    
}
extension UIColor {
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
