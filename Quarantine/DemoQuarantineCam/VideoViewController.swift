//
//  VideoViewController.swift
//  Quarantine
//
//  Created by Fatma Anwar on 3/25/20.
//  Copyright © 2020 Fatma Anwar. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Alamofire
import Photos
import AssetsLibrary
import TransitionButton
import SwiftGifOrigin

class VideoViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private var videoURL: URL
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    
    init(videoURL: URL) {
        self.videoURL = videoURL
        Constants.videoURL = videoURL.absoluteString
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.gray
        
        // Add Player Video View
        player = AVPlayer(url: videoURL)
        playerController = AVPlayerViewController()
        
        guard player != nil && playerController != nil else {
            return
        }
        playerController!.showsPlaybackControls = false
        
        playerController!.player = player!
        self.addChild(playerController!)
        self.view.addSubview(playerController!.view)
        playerController!.view.frame = view.frame
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
        
        // Add Quarantine Text
        let quarantineImageView = UIImageView()
        quarantineImageView.image = UIImage.gif(name: "quarantine_camera")
        self.view.addSubview(quarantineImageView)
        quarantineImageView.backgroundColor = Constants.Background_COLOR
        quarantineImageView.translatesAutoresizingMaskIntoConstraints = false
        let quarantineImageView_TrailingConstraint = NSLayoutConstraint(item: quarantineImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute:NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 10)
        let quarantineImageView_TopConstraint = NSLayoutConstraint(item: quarantineImageView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 10)
        let quarantineImageView_LeadingConstraint = NSLayoutConstraint(item: quarantineImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 10)
        let quarantineImageView_heightConstraint =  NSLayoutConstraint(item: quarantineImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute,multiplier: 1, constant: 129)
        NSLayoutConstraint.activate([quarantineImageView_TrailingConstraint, quarantineImageView_TopConstraint,quarantineImageView_LeadingConstraint,quarantineImageView_heightConstraint])
        quarantineImageView.clipsToBounds = true
        
        // Add Stay Home Text
        let stayHomeImageView = UIImageView()
        stayHomeImageView.image = UIImage.gif(name: "stay_home")
        self.view.addSubview(stayHomeImageView)
        stayHomeImageView.backgroundColor = Constants.Background_COLORwithAlpha
        stayHomeImageView.translatesAutoresizingMaskIntoConstraints = false
        let stayHomeImageView_TrailingConstraint = NSLayoutConstraint(item: stayHomeImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute:NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 10)
        let stayHomeImageView_BottomConstraint = NSLayoutConstraint(item: stayHomeImageView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant:-50)
        let stayHomeImageView_LeadingConstraint = NSLayoutConstraint(item: stayHomeImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 10)
        let stayHomeImageView_heightConstraint =  NSLayoutConstraint(item: stayHomeImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute,multiplier: 1, constant: 70)
        NSLayoutConstraint.activate([stayHomeImageView_TrailingConstraint, stayHomeImageView_BottomConstraint,stayHomeImageView_LeadingConstraint,stayHomeImageView_heightConstraint])
        stayHomeImageView.clipsToBounds = true
        stayHomeImageView.contentMode = .scaleAspectFit
        self.view.layoutIfNeeded()
        
        // Add Cancel Button
        let cancelButton = UIButton(frame: CGRect(x:  20, y: 20, width: 30, height: 30))
        cancelButton.setImage(#imageLiteral(resourceName: "cancel"), for: UIControl.State())
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        self.view.addSubview(cancelButton)
        
        // Add Share Button
        let shareButton = UIButton(frame: CGRect(x:self.view.frame.width - 45 , y: 20, width: 30, height: 30))
        shareButton.setImage(#imageLiteral(resourceName: "share"), for: UIControl.State())
        shareButton.addTarget(self, action: #selector(share), for: .touchUpInside)
        view.addSubview(shareButton)
        
        // Allow background audio to continue to play
        do {
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: .default, options: [])
            } else {
            }
        } catch let error as NSError {
            print(error)
        }
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error as NSError {
            print(error)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player?.play()
    }
    
    @objc func cancel() {
        Constants.hideCounter = true
        Constants.counter = -10
        Constants.startRecord = false
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    func share() {
        //dismiss(animated: true, completion: nil)
        let localVideoPath = videoURL.absoluteString // "your_video_path_here..."
        let videoURL = URL(fileURLWithPath: localVideoPath)
        guard let url = URL(string: localVideoPath) else { return }
        
        var player2 = AVPlayer(url: videoURL)
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        
        activityViewController.completionWithItemsHandler = { activity, success, items, error in
            if !success{
                print("cancelled")
                return
            }
            if activity == UIActivity.ActivityType.postToFacebook {
                print("facebook")
            }            
            if activity == UIActivity.ActivityType.mail {
                print("mail")
            }
        }
        activityViewController.popoverPresentationController?.sourceView = view
        activityViewController.popoverPresentationController?.sourceRect = view.frame
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: CMTime.zero)
            self.player!.play()
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}
