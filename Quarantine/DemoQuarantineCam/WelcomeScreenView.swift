//
//  WelcomeScreenView.swift
//  Quarantine
//
//  Created by Fatma Anwar on 3/29/20.
//  Copyright © 2020 nweave. All rights reserved.
//

import UIKit
import GhostTypewriter
import TransitionButton

class WelcomeScreenView: UIViewController {
    
    @IBOutlet var glitchLabel: GlitchLabel!
    @IBOutlet weak private var titleLabel: TypewriterLabel!
    @IBOutlet weak private var descriptionLabel: TypewriterLabel!
    @IBOutlet weak var quarantineImage: UIImageView!
    private lazy var programmaticLabel: TypewriterLabel = {
        let programmaticLabel = TypewriterLabel()
        programmaticLabel.numberOfLines = 0
        programmaticLabel.lineBreakMode = .byWordWrapping
        let text = "Still not convinced...\n\nWell this label shows support for attributed labels created programmatically rather than via storyboards so maybe that will soothe you."
        let attributedString = NSMutableAttributedString(string: text)
        
        if let programmaticallyTextRange = text.range(of: "programmatically") {
            let programmaticallyTextNSRange = NSRange(programmaticallyTextRange, in: attributedString.string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.green, range: programmaticallyTextNSRange)
        }
        
        if let storyboardsTextRange = text.range(of: "storyboards") {
            let storyboardsTextNSRange = NSRange(storyboardsTextRange, in: attributedString.string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: storyboardsTextNSRange)
        }
        
        programmaticLabel.attributedText = attributedString
        
        return programmaticLabel
    }()
    
    // MARK: - ViewLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        quarantineImage.image = UIImage.gif(name: "quarantine_welcome")
        //    let tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
        //       let tap = UITapGestureRecognizer(target: self, action: #selector(WelcomeScreenView.tapFunction))
        //              titleLabel.isUserInteractionEnabled = true
        //              titleLabel.addGestureRecognizer(tap)
        //
        //
        //        titleLabel.startTypewritingAnimation {
        //                }
        titleLabel.completeTypewritingAnimation()
        
    }
    
    @objc
    func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        titleLabel.restartTypewritingAnimation()
    }
    // MARK: ButtonActions
    
    @IBAction func startButtonPressed(_ sender: Any) {
        let newViewController = self.storyboard!.instantiateViewController(withIdentifier: "CameraHomeView")
        self.present(newViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonAction(_ button: TransitionButton) {
        button.startAnimation() // 2: Then start the animation when the user tap the button
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            
            sleep(1) // 3: Do your networking task or background work here.
            
            DispatchQueue.main.async(execute: { () -> Void in
                // 4: Stop the animation, here you have three options for the `animationStyle` property:
                // .expand: useful when the task has been compeletd successfully and you want to expand the button and transit to another view controller in the completion callback
                // .shake: when you want to reflect to the user that the task did not complete successfly
                // .normal
                button.stopAnimation(animationStyle: .expand, completion: {
                    let newViewController = self.storyboard!.instantiateViewController(withIdentifier: "CameraHomeView")
                    self.present(newViewController, animated: true, completion: nil)
                })
            })
        })
    }
}
