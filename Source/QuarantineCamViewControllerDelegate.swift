//
//  SwiftyCamViewControllerDelegate.swift
//  Quarantine
//
//  Created by Fatma Anwar on 3/25/20.
//  Copyright © 2020 Fatma Anwar. All rights reserved.
//

import UIKit
import AVFoundation

// MARK: Public Protocol Declaration

/// Delegate for SwiftyCamViewController

public protocol QuarantineCamViewControllerDelegate: class {
    
    /**
     SwiftyCamViewControllerDelegate function called when when SwiftyCamViewController session did start running.
     Photos and video capture will be enabled.
     
     - Parameter swiftyCam: Current SwiftyCamViewController
     */
    
    func swiftyCamSessionDidStartRunning(_ swiftyCam: QuarantineViewController)
    
    /**
     SwiftyCamViewControllerDelegate function called when when SwiftyCamViewController session did stops running.
     Photos and video capture will be disabled.
     
     - Parameter swiftyCam: Current SwiftyCamViewController
     */
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: QuarantineViewController)
    
    /**
     SwiftyCamViewControllerDelegate function called when the takePhoto() function is called.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter photo: UIImage captured from the current session
     */
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didTake photo: UIImage)
    
    /**
     SwiftyCamViewControllerDelegate function called when SwiftyCamViewController begins recording video.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter camera: Current camera orientation
     */
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didBeginRecordingVideo camera: QuarantineViewController.CameraSelection)
    
    /**
     SwiftyCamViewControllerDelegate function called when SwiftyCamViewController finishes recording video.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter camera: Current camera orientation
     */
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFinishRecordingVideo camera: QuarantineViewController.CameraSelection)
    
    /**
     SwiftyCamViewControllerDelegate function called when SwiftyCamViewController is done processing video.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter url: URL location of video in temporary directory
     */
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFinishProcessVideoAt url: URL)
    
    
    /**
     SwiftyCamViewControllerDelegate function called when SwiftyCamViewController fails to record a video.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter error: An error object that describes the problem
     */
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFailToRecordVideo error: Error)
    
    /**
     SwiftyCamViewControllerDelegate function called when SwiftyCamViewController switches between front or rear camera.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter camera: Current camera selection
     */
    
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didSwitchCameras camera: QuarantineViewController.CameraSelection)
    
    /**
     SwiftyCamViewControllerDelegate function called when SwiftyCamViewController view is tapped and begins focusing at that point.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter point: Location in view where camera focused
     
     */
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFocusAtPoint point: CGPoint)
    
    /**
     SwiftyCamViewControllerDelegate function called when when SwiftyCamViewController view changes zoom level.
     
     - Parameter swiftyCam: Current SwiftyCamViewController session
     - Parameter zoom: Current zoom level
     */
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didChangeZoomLevel zoom: CGFloat)
    
    /**
     SwiftyCamViewControllerDelegate function called when when SwiftyCamViewController fails to confiture the session.
     
     - Parameter swiftyCam: Current SwiftyCamViewController
     */
    
    func swiftyCamDidFailToConfigure(_ swiftyCam: QuarantineViewController)
    
    /**
     SwiftyCamViewControllerDelegate function called when when SwiftyCamViewController does not have access to camera or microphone.
     
     - Parameter swiftyCam: Current SwiftyCamViewController
     */
    
    func swiftyCamNotAuthorized(_ swiftyCam: QuarantineViewController)
}

public extension QuarantineCamViewControllerDelegate {
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: QuarantineViewController) {
        // Optional
    }
    
    func swiftyCamSessionDidStartRunning(_ swiftyCam: QuarantineViewController) {
        // Optional
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didTake photo: UIImage) {
        // Optional
    }

    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didBeginRecordingVideo camera: QuarantineViewController.CameraSelection) {
        // Optional
    }

    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFinishRecordingVideo camera: QuarantineViewController.CameraSelection) {
        // Optional
    }

    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFinishProcessVideoAt url: URL) {
        // Optional
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFailToRecordVideo error: Error) {
        // Optional
    }
    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didSwitchCameras camera: QuarantineViewController.CameraSelection) {
        // Optional
    }

    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didFocusAtPoint point: CGPoint) {
        // Optional
    }

    
    func swiftyCam(_ swiftyCam: QuarantineViewController, didChangeZoomLevel zoom: CGFloat) {
        // Optional
    }
    
    func swiftyCamDidFailToConfigure(_ swiftyCam: QuarantineViewController) {
        // Optional
    }
    
    func swiftyCamNotAuthorized(_ swiftyCam: QuarantineViewController) {
        // Optional
    }
}



